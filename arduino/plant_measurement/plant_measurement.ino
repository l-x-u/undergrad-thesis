
const int voltagePin = A0;
const double v_ref = 3.3;
const int baud_rate = 9600;
const int adc_bits = 12;
const float v_max = (float)(1 << adc_bits);
const long minute_in_ms = 1000 * 60;
const long interval_ms = minute_in_ms * 5;
const int samples = 50;
const long sample_interval_ms = 100;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  // initialize serial communication line
  Serial.begin(9600);
  // initialize voltage measurement pins
  analogReadResolution(adc_bits); // between 0 and 4096
}

// the loop function runs over and over again forever
void loop() {
  for(int i = 0; i < samples; ++i) {
    record();
    delay(sample_interval_ms);
  }

  delay(interval_ms);
}

void record() {
  int val = analogRead(voltagePin);
  double voltage = val * (v_ref / v_max);
  if(val > v_max / 2) {
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    digitalWrite(LED_BUILTIN, LOW);
  }

  // data is sent over the serial line to a host program that saves the data to CSV.
  Serial.print(voltage);
  Serial.print(",");
  Serial.print(val);
  Serial.print("\n");
}

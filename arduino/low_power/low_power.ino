
const int voltagePin = A0;
const double v_ref = 3.3;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  // initialize serial communication line
  Serial.begin(9600);
  // initialize voltage measurement pins
  analogReadResolution(12); // between 0 and 4096
  
  Serial.print("\nOnline!\n");
}

// the loop function runs over and over again forever
void loop() {


  int val = analogRead(voltagePin);

  double voltage = val * (v_ref / 4096.0);


  if(val > 2048) {
    digitalWrite(LED_BUILTIN, HIGH);
  } else {
    digitalWrite(LED_BUILTIN, LOW);
  }

  Serial.print("Voltage: ");
  Serial.print(voltage);
  Serial.print("V\t");
  Serial.print("Raw: ");
  Serial.print(val);
  Serial.print("\n");
  
  delay(100);
}

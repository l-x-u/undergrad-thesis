import SerialPort from 'serialport'
import log4js from 'log4js'
import Readline from '@serialport/parser-readline'
import fs from 'fs'
import { stdin } from 'process'
import { Transform } from 'stream'

const logger = log4js.getLogger('HOST')
logger.level = 'debug'

const selectPort = async () => {
  const ports = await SerialPort.list()
  logger.debug('Found ports: ' + JSON.stringify(ports, null, 1))
  return ports.find(x => x.manufacturer.startsWith('Arduino'))
}

const transform = new Transform({
  transform: (chunk, _, done) => done(null, new Date().toLocaleDateString('en-GB') + ',' + new Date().toLocaleTimeString('en-GB') + ',' + chunk + '\n')
})

const main = async () => {
  const port = await selectPort()
  const stream = new SerialPort(port.path)
  const parser = stream.pipe(new Readline())
  const now = new Date()
  const output = fs.createWriteStream('./output-' +
    now.getDate() + '-' +
    (now.getMonth() + 1) + '-' +
    now.getFullYear() + ' ' +
    now.getHours() + ' ' +
    now.getMinutes() + ' ' +
    now.getSeconds() + '.csv')
  output.write('date,time,voltage,raw\n')
  stdin.setRawMode(true)
  stdin.resume()
  stdin.on('data', _ => {
    stream.close()
    output.end()
    process.exit()
  })
  parser.pipe(transform).pipe(output)
}

await main()

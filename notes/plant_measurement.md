- light & dark
- temperature

day / night cycle for light vs. dark

- hourly recording
- start at current temperature
- 5 minute sample window
- sample every ten seconds
- record for 2 days and 2 nights
- trimmed median: throw out outlying data then take median
- after recording, switch to lowered light level
- after several days, switch to room temperature, then go to inbetween or hot temperature